const postalCodeRegex = /^[1-9][0-9]{3}[\s]?[A-Za-z]{2}$/i;

export const postalCodeValidator = (value: string) => postalCodeRegex.test(value);
