export interface photonResponse {
    type: string,
    features: [{
        geometry: {
            coordinates: {
                0: number,
                1: number,
            },
            type: string
        },
        type: string,
        properties: {
            osm_id: number,
            osm_type: string,
            extent: {
                0: number,
                1: number,
                2: number,
                3: number,
            },
            country: string,
            osm_key: string,
            city: string,
            street: string,
            osm_value: string,
            postcode: string,
            name: string,
            state: string,
        }
    }]
}